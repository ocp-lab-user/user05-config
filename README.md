This repository contains exercises for Lund & Bendsen training sessions
Questions are written with italics and answers may be written at the line below.

# Pre-setup of lab exercises

Login to Bitbucket by accessing this URL if your are not already logged in:
https://bitbucket.org/erik-lund/exercise-8a/src/master/

You do not need to fork the source project to run these exercises


Description of exercises are here:
https://bitbucket.org/erik-lund/exercise-8a/src/master/
